import { Request, Response } from "express";
import { QueryResult } from 'pg';
import { pool } from '../../db';
import { v4 as uuidv4 } from 'uuid';

export const getManufacturer = async(req: Request, res: Response) : Promise<Response> => {
    try{
        const response: QueryResult<any> = await pool.query('SELECT * FROM manufacturer');
        return res.status(500).json(response.rows);
    } catch (err){
        console.log('err', err);
        return res.status(500).json('Internal Server Error');
      }
};

export const getManufacturerById = async(req: Request, res: Response) : Promise<Response> => {
    const id = req.params.id;
    const response: QueryResult = await pool.query(
        'SELECT * FROM manufacturer WHERE id = $1', [id]
    );
    return res.json(response.rows);
};

export const createManufacturer = async(req: Request, res: Response) : Promise<Response> => {
    const { name } = req.body;
    const idv4 = uuidv4();
    await pool.query(
        'INSERT INTO manufacturer (name, id) VALUES($1, $2)',
        [name, idv4]
    );
    return res.json({
        message: 'Manufacturer created successfully',
        body: { name }
    });
};

export const updateManufacturer = async(req: Request, res: Response) : Promise<Response> => {
    const { name } = req.body;
    const id = req.params.id;

    await pool.query(
        'UPDATE manufacturer SET name = $1 WHERE id = $2',
        [name, id]
    );
    return res.json(`Manufacturer updated successfully`);
};

export const running = async(res: Response) : Promise<Response> => {
    return res.json(`RUNNING`);
}

export const deleteManufacturer = async(req: Request, res: Response): Promise<Response> => {
    const id = req.params.id;
    await pool.query('DELETE FROM manufacturer WHERE id = $1', [id]);
    return res.json(`Manufacturer deleted successfully`);
  };
