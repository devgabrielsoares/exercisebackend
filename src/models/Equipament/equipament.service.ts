import { Request, Response } from "express";
import { QueryResult } from 'pg';
import { pool } from '../../db';
import { v4 as uuidv4 } from 'uuid';

export const getEquipament = async(req: Request, res: Response) : Promise<Response> => {
    try{
        const response: QueryResult<any> = await pool.query('SELECT * FROM equipament');
        return res.status(500).json(response.rows);
    } catch (err){
        console.log('err', err);
        return res.status(500).json('Internal Server Error');
      }
};

export const getEquipamentById = async(req: Request, res: Response) : Promise<Response> => {
    const id = req.params.id;
    const response: QueryResult = await pool.query(
        'SELECT * FROM equipament WHERE id = $1', [id]
    );
    return res.json(response.rows);
};

export const createEquipament = async(req: Request, res: Response) : Promise<Response> => {
    const { model, serialnumber, fk_manufacturer_id } = req.body;
    const idv4 = uuidv4();
    await pool.query(
        'INSERT INTO equipament (model, serialnumber, id, fk_manufacturer_id) VALUES($1, $2, $3, $4)',
        [model, serialnumber, idv4, fk_manufacturer_id]
    );
    return res.json({
        message: 'Equipament created successfully',
        body: { model, serialnumber, fk_manufacturer_id }
    });
};

export const updateEquipament = async(req: Request, res: Response) : Promise<Response> => {
    const { model, serialnumber } = req.body;
    const id = req.params.id;

    await pool.query(
        'UPDATE equipament SET model = $1, serialnumber = $2 WHERE id = $3',
        [model, serialnumber, id]
    );
    return res.json(`Equipament updated successfully`);
};

export const deleteEquipament = async(req: Request, res: Response): Promise<Response> => {
    const id = req.params.id;
    await pool.query('DELETE FROM equipament WHERE id = $1', [id]);
    return res.json(`Equipament deleted successfully`);
  };
