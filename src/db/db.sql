CREATE TABLE manufacturer (
    id uuid NOT NULL,
    name VARCHAR(100),
    PRIMARY KEY (id)
);
CREATE TABLE equipament (
    id uuid NOT NULL,
    fk_manufacturer_id uuid NOT NULL,
    model VARCHAR(100),
    serialNumber VARCHAR(100),
    PRIMARY KEY (id),
    FOREIGN KEY (fk_manufacturer_id) REFERENCES manufacturer(id) ON DELETE CASCADE
);