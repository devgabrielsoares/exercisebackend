import express from 'express';
import router from './routes/index';

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(router);

app.listen(8080, () => {
    console.log('Server is running on port 8080');
});