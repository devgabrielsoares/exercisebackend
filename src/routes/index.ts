import { Router } from "express";

//CRUD FUNCTIONS EQUIPAMENT
import {
    createEquipament,
    deleteEquipament,
    getEquipament,
    getEquipamentById,
    updateEquipament
} from "../models/Equipament/equipament.service";

//CRUD FUNCTIONS MANUFACTURER
import { 
    createManufacturer, 
    deleteManufacturer, 
    getManufacturer, 
    getManufacturerById, 
    running, 
    updateManufacturer 
} from "../models/Manufacturer/manufacturer.service";

const router = Router();

//CRUD ROUTES EQUIPAMENTS
router.get('/equipament', getEquipament);
router.get('/equipament/:id', getEquipamentById);
router.post('/equipament', createEquipament);
router.patch('/equipament/:id', updateEquipament);
router.delete('/equipament/:id', deleteEquipament); 

//CRUD ROUTES MANUFACTURERS
router.get('/manufacturer', getManufacturer);
router.get('/manufacturer/:id', getManufacturerById);
router.post('/manufacturer', createManufacturer);
router.patch('/manufacturer/:id', updateManufacturer);
router.delete('/manufacturer/:id', deleteManufacturer);

export default router;